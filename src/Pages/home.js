import React, { useState, useEffect } from 'react';
import Footer from '../Pages/footer'
import Carousel from 'react-bootstrap/Carousel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight, faChevronUp, faChevronDown } from '@fortawesome/free-solid-svg-icons';
import services from '../assets/images/service_background.png'
import about from '../assets/images/about.png'
import settings from '../assets/images/settings.png'
import person from '../assets/images/person.png'
import tool from '../assets/images/tool.png'
import web from '../assets/images/web.png'
import security from '../assets/images/security.png'
import clients from '../assets/images/clients.png'
import projects from '../assets/images/projects.png'
import experts from '../assets/images/experts.png'
import posts from '../assets/images/posts.png'
import website from '../assets/images/globe.png'
import apple from '../assets/images/apple.png'
import android from '../assets/images/android.png'
import iot from '../assets/images/iot.png'
import requirement from '../assets/images/requirement.png'
import prototype from '../assets/images/prototype.png'
import solution from '../assets/images/solution.png'
import profile from '../assets/images/Profile.png'
import profile_img from '../assets/images/profile_img.png'
import it from '../assets/images/it_management.png'
import technology from '../assets/images/technology.png'
import case_security from '../assets/images/case_security.png'
import web_development from '../assets/images/web_development.png'
import '../assets/css/home.css'

const Home = () => {
    let totalStars = 5;
    const cardsToShow = 2;
    const autoAdvanceTime = 3000;
    const [rating, setRating] = useState(0);
    const [hover, setHover] = useState(0);
    const [currentIndex, setCurrentIndex] = useState(0);
    const cards = [
        { title: 'Alex Rony', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation', profileImg: profile },
        { title: 'Jane Doe', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation', profileImg: profile },
        { title: 'John Smith', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation', profileImg: profile },
        { title: 'Alice Johnson', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation', profileImg: profile },
        { title: 'Bob Brown', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation', profileImg: profile },
        { title: 'Charlie Davis', content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation', profileImg: profile },
    ];

    useEffect(() => {
        const interval = setInterval(() => {
            if (currentIndex < cards.length - cardsToShow) {
                setCurrentIndex((prevIndex) => prevIndex + cardsToShow);
            } else {
                setCurrentIndex(0);
            }
        }, autoAdvanceTime);

        return () => clearInterval(interval);
    }, [currentIndex, cards.length]);

    const handleUpClick = () => {
        if (currentIndex > 0) {
            setCurrentIndex(currentIndex - cardsToShow);
        }
    };

    const handleDownClick = () => {
        if (currentIndex < cards.length - cardsToShow) {
            setCurrentIndex(currentIndex + cardsToShow);
        }
    };

    return (
        <>
            <div>
                <Carousel interval={3000} pause={false}>
                    <Carousel.Item>
                        <img className="d-block w-100" src={services} alt="First slide" />
                        <Carousel.Caption>
                            <p className='services_head'>BEST IT SOLUTION PROVIDER</p>
                            <h1>Excellent IT services for your success</h1>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                            </p>
                            <button className='basic_btn'> Explore More <span className='explore_arrow'><FontAwesomeIcon icon={faArrowRight} /></span></button>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img className="d-block w-100" src={services} alt="Second slide" />
                        <Carousel.Caption>
                            <p className='services_head'>BEST IT SOLUTION PROVIDER</p>
                            <h1>Excellent IT services for your success</h1>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                            </p>
                            <button className='basic_btn'>Explore More <span className='explore_arrow'><FontAwesomeIcon icon={faArrowRight} /></span></button>
                        </Carousel.Caption>
                    </Carousel.Item>
                    <Carousel.Item>
                        <img className="d-block w-100" src={services} alt="Third slide" />
                        <Carousel.Caption>
                            <p className='services_head'>BEST IT SOLUTION PROVIDER</p>
                            <h1>Excellent IT services for your success</h1>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                            </p>
                            <button className='basic_btn'>Explore More <span className='explore_arrow'><FontAwesomeIcon icon={faArrowRight} /></span></button>
                        </Carousel.Caption>
                    </Carousel.Item>
                </Carousel>
            </div>
            <div>
                <h2 className='about_head'>WHAT WE OFFER</h2>
                <p className='about_business'>Excellent It services</p>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-4'>
                            <div className='card business'>
                                <div className='card image'>
                                    <img src={tool} className='card_inner_img'></img>
                                </div>
                                <p className='management'>IT Management</p>
                                <p className='management_content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='card business'>
                                <div className='card image'>
                                    <img src={security} className='card_inner__img'></img>
                                </div>
                                <p className='management'>Cyber Security</p>
                                <p className='management_content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='card business'>
                                <div className='card image'>
                                    <img src={web} className='card_inner_img'></img>
                                </div>
                                <p className='management'>Web Development</p>
                                <p className='management_content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <button className='about_services_btn'>View All Services</button>
                    </div>
                </div>
            </div>
            <div>
                <h2 className='about_head'>ABOUT HYSAS</h2>
                <p className='about_business'>We strive to offer intelligent business solutions</p>
                <div className='container'>
                    <div className='row mt-5'>
                        <div className='col-md-6'>
                            <img src={about} className='about_img' alt="..." />
                        </div>
                        <div className='col-md-6'>
                            <p className='about_section'>Morem ipsum dolor sit amet, consectetur adipiscing elit. Etiam eu turpis molestie, dictum est a, mattis tellus. Sed dignissim, metus nec fringilla accumsan, risus sem sollicitudin lacus, ut interdum tellus elit sed risus. Maecenas eget condimentum velit, sit amet feugiat lectus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.</p>
                            <div className="card_section">
                                <div className='card_inner_section'>
                                    <img src={settings} className='card_img'></img>
                                </div>
                                <div>
                                    <h1 className='services'>Best services</h1>
                                    <p className='services_section'>Lorem ipsum dolor sit amet, consectetur</p>
                                </div>
                                <div className='card_inner_section'>
                                    <img src={person} className='card_img'></img>
                                </div>
                                <div>
                                    <h1 className='support'>24/7 Call support</h1>
                                    <p className='support_section'>Lorem ipsum dolor sit amet, consectetur</p>
                                </div>
                            </div>
                            <div>
                                <button className='about_basic_btn'>Explore More <span><FontAwesomeIcon icon={faArrowRight} /></span></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className='about_image'>
                <div className='row'>
                    <div className='col-md-3 mt-5'>
                        <div className='rating'>
                            <div>
                                <img src={clients} className='rating_img'></img>
                            </div>
                            <div>
                                <p className='rating_content'>6,561 +</p>
                                <p className='rating_content'>Satisfied Clients</p>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-3 mt-5'>
                        <div className='rating'>
                            <div >
                                <img src={projects} className='rating_img'></img>
                            </div>
                            <div>
                                <p className='rating_content'>600 +</p>
                                <p className='rating_content'>Finished Projects</p>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-3 mt-5'>
                        <div className='rating'>
                            <div >
                                <img src={experts} className='rating_img'></img>
                            </div>
                            <div>
                                <p className='rating_content'>100 +</p>
                                <p className='rating_content'>Skilled Experts</p>
                            </div>
                        </div>
                    </div>
                    <div className='col-md-3 mt-5'>
                        <div className='rating'>
                            <div >
                                <img src={posts} className='rating_img'></img>
                            </div>
                            <div>
                                <p className='rating_content'>100 +</p>
                                <p className='rating_content'>Media Posts</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <h2 className='about_head'>FROM OUR CASE STUDIES</h2>
                <p className='about_business'>We delivered best solution</p>
                <div className='container'>
                    <Carousel interval={3000} pause={false}>
                        <Carousel.Item>
                            <div className='row'>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={it} alt="Slide 1" />
                                    <p className='case_studies'>Solution</p>
                                    <p className='case_studies_content'>IT Management</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                                <div className='col-md-3'>
                                    <img className='case_studies_img'  src={technology} alt="Slide 1" />
                                    <p className='case_studies'>Technology</p>
                                    <p className='case_studies_content'>Platform Integration</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={case_security} alt="Slide 1" />
                                    <p className='case_studies'>Security</p>
                                    <p className='case_studies_content'>Network Security</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={web_development} alt="Slide 1" />
                                    <p className='case_studies'>Solution</p>
                                    <p className='case_studies_content'>Web Management</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                            </div>
                        </Carousel.Item>
                        <Carousel.Item>
                            <div className='row'>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={it} alt="Slide 2" />
                                    <p className='case_studies'>Solution</p>
                                    <p className='case_studies_content'>IT Management</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={technology} alt="Slide 2" />
                                    <p className='case_studies'>Technology</p>
                                    <p className='case_studies_content'>Platform Integration</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={case_security} alt="Slide 2" />
                                    <p className='case_studies'>Security</p>
                                    <p className='case_studies_content'>Network Security</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={web_development} alt="Slide 2" />
                                    <p className='case_studies'>Solution</p>
                                    <p className='case_studies_content'>Web Management</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                            </div>
                        </Carousel.Item>
                        <Carousel.Item>
                            <div className='row'>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={it} alt="Slide 2" />
                                    <p className='case_studies'>Solution</p>
                                    <p className='case_studies_content'>IT Management</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={technology} alt="Slide 2" />
                                    <p className='case_studies'>Technology</p>
                                    <p className='case_studies_content'>Platform Integration</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={case_security} alt="Slide 2" />
                                    <p className='case_studies'>Security</p>
                                    <p className='case_studies_content'>Network Security</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                                <div className='col-md-3'>
                                    <img className='case_studies_img' src={web_development} alt="Slide 2" />
                                    <p className='case_studies'>Solution</p>
                                    <p className='case_studies_content'>Web Management</p>
                                    <div className='rectangle'><FontAwesomeIcon icon={faArrowRight} /></div>
                                </div>
                            </div>
                        </Carousel.Item>
                    </Carousel>
                </div>
                <div>
                    <button className='view_all_btn'>View All Cases</button>
                </div>
            </div>
            <div>
                <h2 className='about_head'>OUR OFFERING</h2>
                <p className='about_business'>Enhance And Pioneer Using Technology Trends</p>
                <div className='offer_img'>
                    <div className='container'>
                        <div className='row'>
                            <div className='col-md-3'>
                                <div className='sample'>
                                    <div className="box"></div>
                                    <img src={website} className='offer_web'></img>
                                    <p className='offer_web_content'>Website</p>
                                </div>
                            </div>
                            <div className='col-md-3'>
                                <div className='sample'>
                                    <div className="box"></div>
                                    <img src={android} className='offer_android'></img>
                                    <p className='offer_android_content'>Android</p>
                                </div>
                            </div>
                            <div className='col-md-3'>
                                <div className='sample'>
                                    <div className="box"></div>
                                    <img src={apple} className='offer_apple'></img>
                                    <p className='offer_apple_content'>Apple</p>
                                </div>
                            </div>
                            <div className='col-md-3'>
                                <div className='sample'>
                                    <div className="box"></div>
                                    <img src={iot} className='offer_iot'></img>
                                    <p className='offer_iot_content'>IOT</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <h2 className='about_head'>WORK PROCESS</h2>
                <p className='about_business'>Our Development Process</p>
                <div className='container'>
                    <div className='row mt-5'>
                        <div className='col-md-4'>
                            <div className='card work'>
                                <div className='demo'>
                                    <img src={requirement} className='req_img'></img>
                                    <div className='circle'>1</div>
                                </div>
                                <p className='requirement_head'>Define Requirements</p>
                                <p className='requirement_des'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='card work'>
                                <div className='demo'>
                                    <img src={prototype} className='req_img'></img>
                                    <div className='circle'>2</div>
                                </div>
                                <p className='requirement_head'>Design & Prototyping</p>
                                <p className='requirement_des'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                        <div className='col-md-4'>
                            <div className='card work'>
                                <div className='demo'>
                                    <img src={solution} className='req_img'></img>
                                    <div className='circle'>3</div>
                                </div>
                                <p className='requirement_head'>Final Solution</p>
                                <p className='requirement_des'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <h2 className='about_head'>CLIENT'S REVIEW</h2>
                <p className='about_business'>What they say about us</p>
                <div className='container'>
                    <p className='review_content'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation</p>
                    <div className='row'>
                        <div className='col-md-6'>
                            <div className='form_background'>
                                <div className='form_align'>
                                    <div className="rectangle-box">
                                        <p className='review_des'>Talk to us</p>
                                        <p className='review_des'>How may we help you !</p>
                                        <form>
                                            <div style={{ display: "flex" }}>
                                                <div className="form-group">
                                                    <label>Name</label>
                                                    <input type="text" name="input1" />
                                                </div>
                                                <div className="form-group label">
                                                    <label>Email</label>
                                                    <input type="text" name="input2" />
                                                </div>
                                            </div>
                                            <div style={{ display: "flex" }}>
                                                <div className="form-group">
                                                    <label>Subject</label>
                                                    <input type="text" name="input3" />
                                                </div>
                                                <div className="form-group label">
                                                    <label>Phone Number</label>
                                                    <input type="text" name="input4" />
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <label>Message</label>
                                                <textarea type="text" name="input5" className='review_textarea' />
                                            </div>
                                            <button type="submit" className='submit_btn'>Send Message</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='col-md-6'>
                            <div>
                                <div>
                                    <div>
                                        <button onClick={handleUpClick} className='arrow-btn-up-arrow'><FontAwesomeIcon icon={faChevronUp} /></button>
                                        <button onClick={handleDownClick} className='arrow-btn-down-arrow'><FontAwesomeIcon icon={faChevronDown} /></button>
                                    </div>
                                    {
                                        cards?.slice(currentIndex, currentIndex + cardsToShow).map((data, index) => {
                                            return (
                                                <div className='card review' key={index}>
                                                    <div className='review_section'>
                                                        <img src={profile} className='profile_img'></img>
                                                        <div>
                                                            <p className='review_name'>{data?.title}</p>
                                                            <p className='review_designation'>Web Designer</p>
                                                            <div className="star-rating">
                                                                {[...Array(totalStars)].map((star, index) => {
                                                                    const ratingValue = index + 1;

                                                                    return (
                                                                        <label key={index}>
                                                                            <input
                                                                                type="radio"
                                                                                name="rating"
                                                                                value={ratingValue}
                                                                                onClick={() => setRating(ratingValue)}
                                                                            />
                                                                            <svg
                                                                                className="star"
                                                                                onMouseEnter={() => setHover(ratingValue)}
                                                                                onMouseLeave={() => setHover(0)}
                                                                                fill={ratingValue <= (hover || rating) ? "#3B70F9" : "lightgray"}
                                                                                viewBox="0 0 24 24"
                                                                                width="24"
                                                                                height="24"
                                                                            >
                                                                                <path d="M12 .288l2.833 8.718h9.167l-7.417 5.385 2.833 8.719-7.416-5.384-7.417 5.384 2.833-8.719-7.417-5.385h9.167z" />
                                                                            </svg>
                                                                        </label>
                                                                    );
                                                                })}
                                                            </div>
                                                        </div>
                                                        <div>
                                                            <img src={profile_img} className='profile_shape_img'></img>
                                                            <img src={profile_img} className='profile_shape_sec_img'></img>
                                                        </div>
                                                    </div>
                                                    <p className='review_description'>{data?.content}</p>
                                                </div>
                                            )
                                        })
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <Footer />
            <div className='chat'>
                <div class="elfsight-app-acd43838-6eea-4b74-8710-8e84ed4fe6be" data-elfsight-app-lazy></div>
            </div>
        </>
    );
}

export default Home;