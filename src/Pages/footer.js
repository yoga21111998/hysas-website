import React from 'react'
import Logo from '../assets/images/logo.png'
import facebook from '../assets/images/facebook.png'
import instagram from '../assets/images/insta.png'
import google from '../assets/images/google.png'
import linkedin from '../assets/images/linkedin.png'
import '../assets/css/footer.css'
const footer = () => {

    return (
        <>
            <div className='footer'>
            <div className='footer_circle'></div>
            <div className='footer_small_circle'></div>
                <div className='container'>
                    <div className='row'>
                        <div className='col-md-3 p-0'>
                            <div className='footer_logo'>
                                <img src={Logo}></img>
                                <p className='footer_para'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi</p>
                                <p className='footer_follow'>Follow us on</p>
                                <img src={facebook} className='footer_fb_icon'></img>
                                <img src={google} className='footer_icon'></img>
                                <img src={linkedin} className='footer_icon'></img>
                                <img src={instagram} className='footer_icon'></img>
                            </div>
                        </div>
                        <div className='col-md-3'>
                            <div className='footer_address'>
                                <p className='footer_location'>Location</p>
                                <p className='footer_location_details'>INDIA ( Registered Office )</p>
                                <p className='footer_location_details'>Hysas technologies private Limited, No.61A1 Bolten Puram, 3rd street,</p>
                                <p className='footer_location_details'>Thoothukudi - 628003.</p>
                                <p className='footer_location_details'>Email: Info@hysas.com </p>
                                <p className='footer_location_details'>Tel: +91-637 929 6448</p>
                            </div>
                        </div>
                        <div className='col-md-3'>
                            <div className='footer_second_address'>
                                <p className='footer_location'>Location</p>
                                <p className='footer_location_details'>INDIA ( Development Center )</p>
                                <p className='footer_location_details'>Hysas technologies private Limited, No.17/2 Visalatchi Nagar, 2nd street,</p>
                                <p className='footer_location_details'>Ekkaduthangal Chennai - 600032.</p>
                                <p className='footer_location_details'>Email: Info@hysas.com</p>
                            </div>
                        </div>
                        <div className='col-md-3'>
                            <div className='footer_submit'>
                                <input type="text" className='footer_input' placeholder='Subscribe with us'></input>
                                <button type="submit" className='footer_submit_btn'>Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default footer;