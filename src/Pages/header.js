import React from 'react';
import { NavLink  } from 'react-router-dom';
import Logo from '../assets/images/logo.png'
import '../assets/css/header.css'

const header = () => {
    return (
        <div>
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
                <div class="container-fluid">
                    <div className='logo'>
                    <img src={Logo} alt="My Image" className='logo_img' ></img>
                    </div>
                    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">                     
                        <ul class="navbar-nav ms-auto mt-4">
                            <li class="nav-item">
                                <NavLink  to="/" activeClassName="active" exact className="nav-link">Home</NavLink >
                            </li>
                            <li class="nav-item">
                                <NavLink  to="/corporate" activeClassName="active" className="nav-link">Corporate</NavLink >
                            </li>
                            <li class="nav-item">
                                <NavLink  to="/services" activeClassName="active" className="nav-link">Services</NavLink >
                            </li>
                            <li class="nav-item">
                                <NavLink  to="/products" activeClassName="active" className="nav-link">Products</NavLink >
                            </li>
                            <li class="nav-item">
                                <NavLink  to="/technology" activeClassName="active" className="nav-link">Technology</NavLink >
                            </li>
                            <li class="nav-item">
                                <NavLink  to="/careers" activeClassName="active" className="nav-link">Careers</NavLink >
                            </li>
                            <li class="nav-item">
                                <NavLink  to="/contact" activeClassName="active" className="nav-link">Contact Us</NavLink >
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    );
}

export default header;